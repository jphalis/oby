# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-29 14:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0012_auto_20151221_1957'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='edu_email',
            field=models.EmailField(blank=True, max_length=80, null=True, verbose_name=b'.edu email'),
        ),
    ]
