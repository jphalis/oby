boto==2.40.0
django-storages-redux==1.3
Fabric==1.10.2
psycopg2==2.6.1
